﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingCube : MonoBehaviour
{

    public Vector3 rotation_speed = new Vector3(0,10,0);
    public float minimum_distance = 2;
    public float maximum_distance = 2.5f;
    public GameObject camera = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (camera == null) {
            return;
        }
        float distance = Vector3.Distance(camera.transform.position, this.transform.position);
        //Debug.Log(distance);
        if (distance < maximum_distance) {
            // if distance == 2.5 -> 0%
            // if distance == 2.25 -> 50%
            // if distance == 2 -> 100%
            // if distance < 2 -> 100%
            float percent = 0;
            if (distance < minimum_distance) {
                percent = 1;
            } else {
                percent = 1 - ( (distance - minimum_distance) / (maximum_distance - minimum_distance) );
            }
            Debug.Log(percent);
            transform.Rotate(rotation_speed * percent * Time.deltaTime);
        }
    }
}
