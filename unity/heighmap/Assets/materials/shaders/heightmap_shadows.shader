﻿Shader "Unlit/heightmap_shadows"
{
    Properties
    {
		_ElevationTex("Elevation (RGB)", 2D) = "white" {}
		_ElevationMin("Bottom", Range(-1,1)) = -1.0
		_ElevationMax("Top", Range(-1,1)) = 1.0

        _MainTex ("Texture", 2D) = "white" {}
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
			Tags { "LightMode" = "ForwardBase" }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			sampler2D _ElevationTex;
			float _ElevationMin;
			float _ElevationMax;

            struct appdata
            {
                float4 vertex : POSITION;
				float4 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
				float value = _ElevationMin + tex2Dlod(_ElevationTex, float4(v.uv,0.0,1.0)).x * (_ElevationMax - _ElevationMin);
                o.vertex = UnityObjectToClipPos(v.vertex + v.normal.xyz * value);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
