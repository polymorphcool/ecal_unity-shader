﻿Shader "Custom/Heightmap"
{
    Properties
    {
		// elevation
		_ElevationTex("Elevation (RGB)", 2D) = "white" {}
		_ElevationMin("Bottom", Range(-1000,1000)) = -1.0
		_ElevationMax("Top", Range(-1000,1000)) = 1.0

		_NormalTex("Normal (RGB)", 2D) = "normal" {}

        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
		SubShader{
			Tags { "RenderType" = "Opaque" }

			CGPROGRAM
			//Notice the "vertex:vert" at the end of the next line
			#pragma surface surf Standard fullforwardshadows vertex:vert

			sampler2D _MainTex;
			sampler2D _ElevationTex;
			sampler2D _NormalTex;
			float _ElevationMin;
			float _ElevationMax;
			
			struct Input {
				float2 uv_MainTex;
				float2 uv_ElevationTex;
				float2 uv_NormalTex;
				float displacement;
			};

			fixed4 _Color;

			void vert(inout appdata_full v, out Input o) {
				//float value =tex2Dlod(_ElevationTex, v.texcoord).x * (_ElevationMax - _ElevationMin);
				//v.vertex.xyz += v.normal.xyz * value;
				UNITY_INITIALIZE_OUTPUT(Input, o);
				float value = _ElevationMin + tex2Dlod(_ElevationTex, v.texcoord).x * (_ElevationMax - _ElevationMin);
				v.vertex.xyz += v.normal.xyz * value;
				o.displacement = value;
			}

			void surf(Input IN, inout SurfaceOutputStandard o) {
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				o.Alpha = c.a;
			}
			ENDCG
		}
		FallBack "Diffuse"
}