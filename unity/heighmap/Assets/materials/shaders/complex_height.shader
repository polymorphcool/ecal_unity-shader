﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "ECAL/complex_height"
{

	Properties{

		// elevation
		_ElevationTex("Elevation (RGB)", 2D) = "white" {}
		_ElevationMin("Bottom", Range(-1,1)) = -1.0
		_ElevationMax("Top", Range(-1,1)) = 1.0
		_Thickness("Thickness", Range(0,1)) = 0.1
		
		_ElevationSmooth("Elevation Smooth", Range(0,0.1)) = 0.01
		_NormalSmooth("Normal Smooth", Range(0,0.1)) = 0.01

		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_BumpScale("Normal ", Float) = 1
		_Glossiness("Glossiness", Range(0,1)) = 0.5
		_MetallicGlossMap("Metallic", 2D) = "black" {}
		_Metallic("Metallic", Range(0,1)) = 0.0
		_OcclusionMap("OcclusionMap", 2D) = "white" {}
		_OcclusionStrength("Occlusion Strength", Float) = 1

		_Emissive("Emissive", Color) = (0,0,0,1)

	}
		SubShader{

			Tags { "RenderType" = "Geometry" }
			LOD 200

			//Back regular pass
			ZWrite true
			Cull back
			ZTest true

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			//#pragma surface surf Standard fullforwardshadows vertex:vert
			#pragma surface surf Standard vertex:vert addshadow

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0
			#pragma glsl

			sampler2D _MainTex;
			sampler2D _OcclusionMap;
			sampler2D _BumpMap;
			sampler2D _MetallicGlossMap;
			sampler2D _HeightMap;

			sampler2D _ElevationTex;
			uniform float4 _ElevationTex_ST;
			uniform float _ElevationMin;
			uniform float _ElevationMax;
			uniform float _Thickness;
			uniform float _ElevationSmooth;
			uniform float _NormalSmooth;

			struct Input {
				float2 uv_MainTex;
				float2 uv_BumpMap;
				float3 normal;
			};

			half _Glossiness;
			half _Metallic;
			half _BumpScale;
			half _OcclusionStrength;
			fixed4 _Color;
			fixed4 _Emissive;
			
			float get_depth(sampler2D tex, float2 uv) {
				float4 elev = tex2Dlod(_ElevationTex, float4(uv+float2(-_ElevationSmooth,-_ElevationSmooth), 0, 0)) * 0.125;
				elev += tex2Dlod(_ElevationTex, float4(uv + float2(_ElevationSmooth, -_ElevationSmooth), 0, 0)) * 0.125;
				elev += tex2Dlod(_ElevationTex, float4(uv + float2(_ElevationSmooth, _ElevationSmooth), 0, 0)) * 0.125;
				elev += tex2Dlod(_ElevationTex, float4(uv + float2(-_ElevationSmooth, _ElevationSmooth), 0, 0)) * 0.125;
				elev += tex2Dlod(_ElevationTex, float4(uv, 0, 0)) * 0.5;
				return _Thickness * 0.5 + _ElevationMin + elev.r * (_ElevationMax - _ElevationMin);
			}

			void vert(inout appdata_full v, out Input o) {
				UNITY_INITIALIZE_OUTPUT(Input, o);
				float2 euv = v.texcoord.xy * _ElevationTex_ST.xy + _ElevationTex_ST.zw;
				//float4 elev = tex2Dlod(_ElevationTex, float4(euv, 0, 0));
				//float4 elev = height_smoothing( euv );
				//v.vertex.z += _Thickness * 0.5 + _ElevationMin + elev.r * (_ElevationMax - _ElevationMin);
				v.vertex.z += get_depth(_ElevationTex, euv);
				float2 e = float2(_NormalSmooth,0);
				o.normal = normalize(
					float3(
					get_depth(_ElevationTex, euv - e ) - get_depth(_ElevationTex, euv + e ),
					get_depth(_ElevationTex, euv - e.yx ) - get_depth(_ElevationTex, euv + e.yx ),
					2.0 * e.x
					));
				//o.normal = mul(unity_ObjectToWorld, float4(o.normal, 1)).xyz;
			}

			void surf(Input IN, inout SurfaceOutputStandard o) {
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				fixed4 gloss = tex2D(_MetallicGlossMap, IN.uv_MainTex);
				o.Metallic = gloss.r * _Metallic;
				o.Smoothness = gloss.a * _Glossiness;
				if (_NormalSmooth == 0) {
					o.Normal = UnpackScaleNormal(tex2D(_BumpMap, IN.uv_BumpMap), _BumpScale);
				} else {
					o.Normal = IN.normal;
				}
				o.Occlusion = tex2D(_OcclusionMap, IN.uv_MainTex) * _OcclusionStrength;
				o.Alpha = c.a;
				o.Emission = c.rgb * _Emissive;
			}
			ENDCG

			//FRONT pass
			//ZWrite true
			//Cull front
			//ZTest true

			//CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			//#pragma surface surf Standard fullforwardshadows vertex:vert
			//#pragma surface surf Standard vertex:vert // addshadow

			// Use shader model 3.0 target, to get nicer looking lighting
			//#pragma target 3.0
			//#pragma glsl

			//sampler2D _MainTex;
			//sampler2D _OcclusionMap;
			//sampler2D _BumpMap;
			//sampler2D _MetallicGlossMap;
			//sampler2D _HeightMap;

			//sampler2D _ElevationTex;
			//uniform float4 _ElevationTex_ST;
			//uniform float _ElevationMin;
			//uniform float _ElevationMax;
			//uniform float _Thickness;

			//struct Input {
			//	float2 uv_MainTex;
			//};

			//half _Glossiness;
			//half _Metallic;
			//half _BumpScale;
			//half _OcclusionStrength;
			//fixed4 _Color;

			//void vert(inout appdata_full v, out Input o) {
			//	UNITY_INITIALIZE_OUTPUT(Input, o);
			//	float2 euv = v.texcoord.xy * _ElevationTex_ST.xy + _ElevationTex_ST.zw;
			//	float4 h = tex2Dlod(_ElevationTex, float4(euv, 0, 0));
			//	v.vertex.z += _Thickness * -0.5 + _ElevationMin + h.r * (_ElevationMax - _ElevationMin);
			//}

			//void surf(Input IN, inout SurfaceOutputStandard o) {
			//	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			//	o.Albedo = c.rgb;
			//	fixed4 gloss = tex2D(_MetallicGlossMap, IN.uv_MainTex);
			//	o.Metallic = gloss.r * _Metallic;
			//	o.Smoothness = gloss.a * _Glossiness;
			//	o.Normal = UnpackScaleNormal(tex2D(_BumpMap, IN.uv_MainTex), _BumpScale) * -1;
			//	o.Occlusion = tex2D(_OcclusionMap, IN.uv_MainTex) * _OcclusionStrength;
			//	o.Alpha = c.a;
			//}
			//ENDCG

		}

		FallBack "VertexLit"
}