﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{

    public float walkSpeed = 2.0f;
    public float horizontalSpeed = 2.0f;
    private bool move_forward = false;
    private bool move_backward = false;
    private Vector3 foward = new Vector3(0,0,1);
    private float currentSpeed = 0;
    private float targetSpeed = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            move_forward = true;
            targetSpeed = walkSpeed;
        } else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            move_forward = false;
            targetSpeed = 0;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            move_backward = true;
            targetSpeed = walkSpeed;
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            move_backward = false;
            targetSpeed = 0;
        }

        Vector3 position = this.transform.position;
        float motion = 0;
        if (move_forward) {
            motion += 1;
        }
        if (move_backward)
        {
            motion -= 1;
        }
        if (currentSpeed != targetSpeed) {
            currentSpeed += (targetSpeed - currentSpeed) * Time.deltaTime;
        }
        this.transform.position += foward * motion * currentSpeed * Time.deltaTime;

        float h = horizontalSpeed * Input.GetAxis("Mouse X");
        transform.Rotate(0, h, 0);

        foward.z = Mathf.Cos(transform.rotation.ToEuler().y);
        foward.x = Mathf.Sin(transform.rotation.ToEuler().y);

    }
}
